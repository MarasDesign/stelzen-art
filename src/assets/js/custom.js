$(document).ready(function () {

  $("body.impressum nav a").each(function(){
    var oldhref = $(this).attr("href");
    var newhref = "/";
    $(this).attr("href", newhref + oldhref);
    $(this).removeClass("aktiv")
  })

  $(".hamburger").click(function(){
    $(this).toggleClass("open");
     $("header").toggleClass("open");
  });

  $("nav").click(function(){
    $("header").toggleClass("open");
    $(".hamburger").toggleClass("open");
  });

  $(window).scroll(function() {
    if ($(window).scrollTop() >= $('header').height() - 100) {
      $('header').addClass('sticky');
    } else {
      $('header').removeClass('sticky');
    }
  });

  $('a[href^="#"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }
  });

  $(function(){
       $("ul#menu li a").each(function(){
               if ($(this).attr("href") == window.location.pathname){
                       $(this).addClass("aktiv");
                       $(this).parent().parent().parent().find(">a").addClass("aktiv");
               }
       });
  });
  (function ( $ ) {
    //Make your content a heroe
    $.fn.transformHeroes = function() {
        //Variables
        var perspective = '500px',
        delta = 20,
        width = this.width(),
        height = this.height(),
        midWidth = width / 2,
        midHeight = height / 2;
        //Events
        this.on({
          mousemove: function(e) {
            var pos = $(this).offset(),
            cursPosX = e.pageX - pos.left,
            cursPosY = e.pageY - pos.top,
            cursCenterX = midWidth - cursPosX,
            cursCenterY = midHeight - cursPosY;

            $(this).css('transform','perspective(' + perspective + ') rotateX('+ (cursCenterY / delta) +'deg) rotateY('+ -(cursCenterX / delta) +'deg)');
            $(this).removeClass('is-out');
          },
          mouseleave: function() {
            $(this).addClass('is-out');
          }
        });
        //Return
        return this;
    };
  }( jQuery ));

  //Set plugin on cards
  $('.card').transformHeroes();

  var background = $(".background img").prop("src");
  $(".background").css("background", "url(" + background + ")").css("background-size", "cover").css("background-position", "center");
  $(".background img").hide();
  $(".background").parent().addClass("backgroundWrapper"); 

  var bgSrc = $(".pBG img").prop("src");
  $(".pBG").css("background", "url(" + bgSrc + ")").css("background-size", "cover").css("background-position", "center");
  $(".pBG img").hide();


  function parallaxBG() {
    var $bg = document.getElementById("pBG1");

    var yPos = window.pageYOffset / $bg.dataset.speed;
    yPos = -yPos;
    
    var coords = '0% '+ yPos + 'px';
    
    $bg.style.backgroundPosition = coords;
  }

  window.addEventListener("scroll", function(){
    parallaxBG(); 
  });

  var lightbox = $('.gallery a').simpleLightbox();

  if (!$("iframe").length){
    $("#videos, .videosBtn").css("display", "none");
  }

});

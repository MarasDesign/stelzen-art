var gulp = require('gulp');
var panini = require('panini');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync');

gulp.task('html', function() {
  gulp.src('src/pages/**/*.html')
    .pipe(panini({
      root: 'src/pages/',
      layouts: 'src/layout/',
      partials: 'src/partials/',
      helpers: 'src/helpers/',
      data: 'src/data/'
    }))
    .pipe(gulp.dest('build'));
});

gulp.task('sass', function() {
  gulp.src('src/assets/style/*')
  .pipe(sass().on('error', sass.logError))
  .pipe(autoprefixer({
    browsers: ['last 2 versions'],
    cascade: false
  }))
  .pipe(gulp.dest('build/assets/style/'))
});

gulp.task('img', function() {
  gulp.src('src/assets/img/**')
  .pipe(gulp.dest('build/assets/img/'))
});

gulp.task('js', function () {
  gulp.src('src/assets/js/*.js')
  .pipe(uglify())
  .pipe(concat('all.js'))
  .pipe(gulp.dest('build/assets/js'));
});

gulp.task('vendor', function () {
  gulp.src('src/assets/js/vendor/*.js')
  .pipe(uglify())
  .pipe(concat('vendor.js'))
  .pipe(gulp.dest('build/assets/js'));
});

gulp.task('fonts', function () {
  gulp.src('src/assets/fonts/*')
  .pipe(gulp.dest('build/assets/fonts'));
});

gulp.task('browserSync', function() {
   browserSync.init({
      server: {
         baseDir: 'build/'
      }
   });
});

gulp.task('watch', function() {
  gulp.watch('src/assets/style/*', ['sass']);
  gulp.watch('build/assets/js', ['js']);
  gulp.watch('src/**/*.html', [panini.refresh]);
});

gulp.task('default', ['browserSync', 'html', 'js', 'vendor', 'fonts', 'sass', 'img', 'watch']);

